/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.mid_alg8;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class ALG8 {
    public static void main(String[] args) {
        
        long start = System.nanoTime();
        
        Scanner kb = new Scanner(System.in);
        
        int num = kb.nextInt();
        int arr[] = new int[num];
        System.out.print("Size of Array: ");
        System.out.println(num);
        
        System.out.print("Original Array : ");
        for(int i = 0; i < num; i++) {
            arr[i] = kb.nextInt();
            System.out.print(arr[i] + " ");
        }
        
        System.out.println();
        
        System.out.print("Reverse Array : ");
        for(int i = num-1 ; i >= 0; i--) {
            System.out.print(arr[i] + " ");
        }
        
        System.out.println();
        
        long end = System.nanoTime();
        System.out.println("Running Time of Algorithm is " + (end - start) * 1E-9 + " secs.");
        
        
    }
}
